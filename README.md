
<p align="center"><img src="animation_spaceship.gif" alt="Logo" width="100"></p>

<h1 align = 'center'>µ_Space</h1> 


**Micro_Space** est le fruit d'une collaboration entre plusieurs étudiants de **l'ENSSAT**, dans le cadre du module de **Projet Microcontrôleur**. Notre objectif avec ce projet est de créer un jeu hybride, combinant l'excitation effrénée de Subway Surfers avec la précision rythmique de Piano Tiles. Le jeu est conçu pour fonctionner parfaitement sur la carte LPC_17xx (_OPEN 1768_).
> **Plongez dans notre univers, plongez dans Micro_Space.**


## Pour commencer

Pour télécharger le repo : 
```console
git clone https://gitlab.enssat.fr/PIRCA/micro-space.git
```

## Obligatoire

**Veuillez réaliser les commandes ci-dessous que lorsque vous êtes dans le répertoire du projet :**

Par exemple : 
```console
Z:/Projet/micro_space
```

Pour se diriger utiliser le terminal `Git Bash` et non Cmd avec les commandes classiques : cd, ls, cd ..

**Veuillez toujours vous assurer d'être dans la bonne branche lorsque vous réalisez un commit ou un push !** 
Pour cela :

```console 
git branch 
```

## Comment commit ? 

Pour ajouter vos changements avant de faire un commit (en supposant que tous vos fichiers modifiées sont dans le dossier User) : 

```console 
git add User/* 
```

Lorsque vous voulez réaliser un commit **TOUJOURS METTRE UN COMMENTAIRE UTILE** :

```console 
git commit -m "Début des capteurs et réalisation d'un timer à part"
```

Ce qu'il ne faut surtout pas faire :

```console 
git commit
```
Ou pire encore : 

```console 
git commit -m "test"
```

## Comment push ?

Avant de push, réaliser la partie **Comment commit ?** en tenant compte de **Obligatoire** puis :

```console 
git push https://gitlab.enssat.fr/PIRCA/micro-space.git
```

Il est conséillé de réalier un pull pour s'assurer d'avoir les mêmes fichier que sur Gitlab en local : 

```console 
git pull https://gitlab.enssat.fr/PIRCA/micro-space.git
```

## Comment merge ? 
Allez dans la branche dans laquelle vous voulez merge et tapez:

```console 
git merge [origin/nom_branche] 
```

Cela merge la copie du depôt distant qui est situé en local avec votre branche locale. Donc faites un git pull avant pour merge avec la bonne version.
Sinon appelez Sami 1A.




## Fabriqué avec

* [Keil MicroVision](https://www.keil.com/download/) - Compilateur et Simulateur pour Microcontrôleur

## Auteurs

* **Romain Gueritault** - _Chef de projet, Responsable Timer et Capteur_
* **George Pirca** - _Responsable affichage et git_
* **Dania Tonta** - _Responsable Musique_
* **Benjamin Audouys** - _Responsable Bluetooth_
* **Antoine Coutay** - _Coordianteur du projet - Microcontrôleur_
* **Robin Guerzaguet** - _Coordinateur du projet - Git_



