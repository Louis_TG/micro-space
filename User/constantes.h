#ifndef CONSTANTES_H
#define CONSTANTES_H

#include "lpc_types.h"

// Palette de couleurs type Gameboy, teintes de vert
//#define COLOR_1 0x08c4														// Black
//#define COLOR_2 0x334a														// Dark green
//#define COLOR_3 0x85ed														// Green slime
//#define COLOR_4 0xdfb9														// Light green slime

#define COLOR_1 0x00e5														// Black
#define COLOR_2 0x2c07														// Dark green
#define COLOR_3 0xa688														// Green slime
#define COLOR_4 0xf7f6														// Light green slime

// Palette de couleurs type Gwen 
//#define COLOR_1 0x180f														// Black
//#define COLOR_2 0xf050														// Dark green
//#define COLOR_3 0xfcb0														// Green slime
//#define COLOR_4 0xffbb														// Light green slime

#define backgroundColor 3
#define screenWidth 240									// Width of the screen
#define screenHeight 320								// Height of the screen

#define TC0 *(uint32_t *)0x40004008															// Pointeur vers le registre timer counter du timmer 0
#define ADC_CONTROL_REGISTER *(uint32_t *)0x40034000						// Pointeur vers le registre de controle de l'ADC
#define ADC_GLOBAL_DATA_REGISTER *(uint32_t *)0x40034004				// Pointeur vers le registre de data de l'ADC
#define MATCH21 *(uint32_t *)0x4009001C													// Pointeur vers le registre qui contient la valeur de match du timer 2 channel 1
#define TEMP_MS_CAPTEUR 10																			// Temps entre chaque acquisition des capteurs
#define TEMP_MS_MANA 500																				// Temps entre chaque incr�mentation pour le mana et le score
#define NOMBRE_SORT 20																					// Taille de l'espace memoire qui permet de stocker les sorts en jeu

struct type_element_historique{unsigned char id_sort; int hauteur; unsigned char var_affichage;};
typedef struct type_element_historique el_histo;	// Un element de l'historique est un triplet identifiant du sort (position laterale), hauteur du sort (position verticale), choix du design d'affichage

#endif
