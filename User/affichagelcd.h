#ifndef AFFICHAGELCD_H
#define AFFICHAGELCD_H

#define COL1 8
#define COL2 64
#define COL3 120
#define COL4 176

typedef enum {NO_SYM, V_SYM, H_SYM} SYM;

char est_dans_ecran(int x, int y);
void effacer_buffer(void);
void charger_GRAM(void);
void dessiner_tile(int X, int Y, int N, SYM S);
void dessiner_tile(int X, int Y, int N, SYM S);
void afficher_tilemap(void);
void dessiner_george(unsigned char pos ,int i);
void afficher_historique(void);
void effacer_buffer_header(void);
void afficher_score(int score);
void dessiner_explosion(int X, int Y, int i);

#endif
