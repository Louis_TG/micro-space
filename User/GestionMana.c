#include "GestionMana.h"
#include <stdio.h>
#include "global.h"
#include "historique.h"
#include "lpc17xx_uart.h"	// UART pour le bluetooth

// Diminution du mana selon l'attaque s�lectionn�e
void DiminutionMana(){
	if(espacement_sort()){
	  switch (MessageRecu){	// Diminuer la valeur du mana selon la valeur de MessageRecu
		  case '2':
			  if (mana >= 2){
				  mana = mana - 2;
					sort = 2;
				}
			  break;
			case '4':
				if (mana >= 4){
				  sort = 4;
					mana = mana - 4;
				}
				break;
			case '7':
			  if (mana >= 7){
          sort = 7;
					mana = mana - 7;
			  }
		  	break;
			case 's':
				if (mana >= 16){
		  		sort = 16;
					mana = mana - 16;
				}
				break;
    }
	}
	AffichageMana();
}

// Affichage de la barre du mana avec la nouvelle valeur
void AffichageMana(){
	sprintf(MessageEnvoye,"*G%d*",mana);
	UART_Send(LPC_UART0, MessageEnvoye, 64, NONE_BLOCKING) ;
}
