#include "historique.h"
#include "touch\sort.h"

void init_histo(){
  char i;
	el_histo temp;
	temp.id_sort = 0;
  temp.hauteur = 0;
	for(i = 0; i < NOMBRE_SORT; i++){historique[i] = temp;}
}

void ajouter_histo(el_histo ptit_nouveau){
  char i;
	i = 0;
	while(historique[i].id_sort != 0 && i < NOMBRE_SORT){i++;}
	if(i < NOMBRE_SORT){historique[i] = ptit_nouveau;}
}

void maj(){
  char i;
	for(i = 0; i < NOMBRE_SORT; i++){
	  if(historique[i].hauteur > 319){
		  el_histo temp;
	    temp.id_sort = 0;
			historique[i] = temp;
		}
		else{historique[i].hauteur += 4;}
	}
}

void collision(){
  char i;
	for(i = 0; i < NOMBRE_SORT; i++){
		char id;
		int pos;
		id = historique[i].id_sort;
		pos = historique[i].hauteur;
		if(pos > 216 && pos < 285){
		  if(((position_joueur < 63) && (id == 24 || id == 44 || id == 45 || id == 46 || id == 72 || id == 73 || id == 74)) ||
         ((position_joueur > 41) && (position_joueur < 119) && (id == 23 || id == 42 || id == 43 || id == 46 || id == 71 || id == 73 || id == 74)) ||
         ((position_joueur > 97) && (position_joueur < 175) && (id == 22 || id == 41 || id == 43 || id == 45 || id == 71 || id == 72 || id == 74)) ||
         ((position_joueur > 153) && (id == 21 || id == 41 || id == 42 || id == 44 || id == 71 || id == 72 || id == 73))){crash = TRUE;}
		}
  }
}

void ajout_nouveau_sort(){
	if(sort != 0){
	  el_histo metaSort;
		version_sort();
	  if(sort < 100){
		  metaSort.id_sort = sort;
	    metaSort.hauteur = -16;
		  metaSort.var_affichage = TC0 % 2;
		  ajouter_histo(metaSort);
	  }
		if(sort == 162){
		  metaSort.id_sort = 72;
			metaSort.hauteur = -16;
		  metaSort.var_affichage = TC0 % 2;
			ajouter_histo(metaSort);
			metaSort.id_sort = 74;
			metaSort.hauteur = -146;
	    metaSort.var_affichage = TC0 % 2;
			ajouter_histo(metaSort);
			metaSort.id_sort = 72;
			metaSort.hauteur = -276;
			metaSort.var_affichage = TC0 % 2;
			ajouter_histo(metaSort);
		  }
		sort = 0;
	}
}

Bool espacement_sort(){
  int maxi;
	char i;
	maxi = 150;  // valeur arbitraire bien au dessus de la taille d'un obstacle
	for(i = 0; i < NOMBRE_SORT; i++){
    if(historique[i].id_sort != 0){
		  if(historique[i].hauteur < maxi){maxi = historique[i].hauteur;}
		}
	}
	return (Bool)(maxi >= 24); 
}
