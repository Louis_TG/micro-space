#ifndef GLOBALDEC_H
#define GLOBALDEC_H

#include "constantes.h"

Bool Flag_capteur;														// Flag pour l'echantillonage des capteurs d'humidite et de contact
Bool Flag_affichage;													// Flag pour declencher un nouvel affichage
Bool Flag_mana;																// Flag pour l'affichage du mana
Bool Flag_message;														// Flag pour traiter un message bluetooth recu
Bool Capteur_contact; 												// Capteur de contact appuye ou non
Bool Capteur_moisissure; 											// Capteur de moisisure appuye ou non
Bool vitesse_augmenter;												// Pour savoir quand la vitesse est doublee
Bool crash;																		// Pour savoir quand la partie est finie
unsigned char mana;														// Valeur du mana
unsigned short score;													// Valeur du score
unsigned char position_joueur; 								// Position du vaisseau en pixel (8 a 208)
unsigned char sort;														// Cout du sort lance puis identifiant de ce sort
unsigned char incrementeur_capteur_mana; 			// Permet la gestion d'une interruption pour les capteurs et le mana avec UN SEUL timer
char MessageRecu;															// Variable pour stocker le message recu (sort lance)
char MessageEnvoye[10];												// Variable pour stocker le message envoye (valeur de mana)
char Frame_buffer[(240 / 4) * 320];						// Buffer pour l'affichage LCD
float vitesse_musique;												// Permet d'accelerer la musique si la vitesse est doublee
el_histo historique[NOMBRE_SORT];							// Permet de garder en memoire les sorts pour gerer l'affichage et les collisions

#endif
