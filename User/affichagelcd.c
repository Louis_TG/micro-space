#include "affichagelcd.h"
#include "touch\ili_lcd_general.h"			// write_data
#include "global.h"
#include "images.h"


unsigned short COLORS[4] = {COLOR_1, COLOR_2, COLOR_3, COLOR_4}; // Couleurs de l'écran LCD
char final[16];

/**
 * Cette fonction prend en entrée 4 char dont seul les 2 premiers bits
 * sont remplis et rend un char contenant les 8 bits des entrées décalées
 *
 * @param val1
 * @param val2
 * @param val3
 * @param val4
 * @return (val1 << 6) | (val2 << 4) | (val3 << 2) | val4;  
 * 		   Décalage de bits qui donne val1.val2.val3.val4 
 */
unsigned char packValues(unsigned char val1, unsigned char val2, unsigned char val3, unsigned char val4)
{
	return (val1 << 6) | (val2 << 4) | (val3 << 2) | val4;
}

/**
 * Décompresse une valeur empaquetée en fonction de la position spécifiée.
 *
 * @param packedValue La valeur empaquetée à décompresser.
 * @param val Pointeur vers une variable où la valeur décompressée sera stockée.
 * @param pos La position dans la valeur empaquetée à décompresser. Doit être entre 1 et 4.
 *            1 signifie les deux bits les plus significatifs, 4 signifie les deux bits les moins significatifs.
 */
void unpackValues(unsigned char packedValue, unsigned char *val, char pos) {
    *val = (packedValue >> (8 - pos * 2)) & 0x03;
}

/**
 * Inverse l'ordre des bits dans un caractère.
 *
 * @param entree Le caractère dont les bits doivent être inversés.
 * @return Le caractère d'entrée avec l'ordre des bits inversé.
 */
char inverser_bits(char entree)
{
	return ((entree & (3 << 4)) >> 2) | ((entree & (3 << 2)) << 2) | (entree >> 6) | (entree << 6);
}

/**
 * Vérifie si un point est dans les limites de l'écran.
 *
 * @param x La coordonnée x du point.
 * @param y La coordonnée y du point.
 * @return 1 si le point est dans les limites de l'écran, 0 sinon.
 */
char est_dans_ecran(int x, int y)
{
	return ((x >= 0 && x < 240) && (y >= 24 && y < 320));
}

/**
 * Efface le buffer d'affichage en le remplissant avec la couleur d'arrière-plan.
 *
 * Utilise la fonction packValues pour empaqueter la couleur d'arrière-plan en un seul caractère.
 * Parcourt ensuite le buffer d'affichage et remplace chaque pixel par la couleur d'arrière-plan.
 */
void effacer_buffer()
{
	short h,w;
	char color = packValues(backgroundColor, backgroundColor, backgroundColor, backgroundColor);
	for (h = 24; h < 320;h++){
		for (w = 0; w < 240/4; w++){
			Frame_buffer[w + h * 240 / 4] = color;
		}
	}
}

/**
 * Récupère une tuile spécifique à partir d'une carte de tuiles et la stocke dans le tableau final.
 *
 * @param N L'index de la tuile à récupérer.
 * @param S La symetrie de la tuile pour le récuperer dans le bon sens.
 *
 * La fonction parcourt la carte de tuiles et récupère la tuile spécifiée.
 * En fonction du symbole S, la tuile est récupérée de différentes manières :
 * - Si S est 1, les bits de la tuile sont inversés.
 * - Si S est 2, l'ordre des lignes de la tuile est inversé.
 * - Sinon, la tuile est récupérée telle quelle.
 */
void recuperer_tile(int N, SYM S)
{
	int M = (N / 30) * 480 + 2 * (N % 30);
	char k;
	int E;
	int K;
	
	for (k = 0; k < 16; k++)
	{
		if (S == 1)
		{
			final[k] = inverser_bits(image_data_Tile_map_lois[M + 60 * (k / 2) + ((k+1) % 2)]);
			final[k + 1] = inverser_bits(image_data_Tile_map_lois[M + 60 * (k / 2) + (k % 2)]);
		}
		else if (S == 2)
		{
			K = (15-k);
			E =  K - (K%2) + (K+1)%2;
			final[k] = image_data_Tile_map_lois[M + 60 * (E / 2) + (E % 2)];
		}
		else
		{
			final[k] = image_data_Tile_map_lois[M + 60 * (k / 2) + (k % 2)];
		}
	}
}

/**
 * Dessine un sprite à une position spécifiée sur l'écran.
 *
 * @param S Le sprite à dessiner.
 * @param X La coordonnée x où le sprite doit être dessiné.
 * @param Y La coordonnée y où le sprite doit être dessiné.
 * @param wrap Si TRUE, le sprite sera enveloppé autour de l'écran si il dépasse les limites.
 *
 * La fonction parcourt chaque tuile du sprite, récupère la tuile correspondante de la carte de tuiles,
 * puis dessine chaque pixel de la tuile à la position appropriée sur l'écran.
 * Si le paramètre wrap est TRUE, les coordonnées x et y sont modulées par la largeur et la hauteur de l'écran,
 * ce qui permet au sprite de se "envelopper" autour de l'écran si il dépasse les limites.
 */
void dessiner_sprite_george(const SPRITE S, int X, int Y, Bool wrap)
{
	int h, w, wrappedX, wrappedY, W, H;
	char pos, remainder, shift;
	unsigned char color;
	unsigned int frameIndex;

	int i, j;
	int index_spacecraft_tile;
	short number_tile;
	SYM sym;

	for (j = 0; j < S.tile_hauteur; j++)
	{
		for (i = 0; i < S.tile_largeur; i++)
		{
			index_spacecraft_tile = i + j * S.tile_largeur;
			number_tile = S.sprite_tiles[index_spacecraft_tile];
			sym = (SYM)S.sym_tiles[index_spacecraft_tile];
			recuperer_tile(number_tile, sym);
			
			for (h = 0; h < 8; h++)
			{
				
				for (w = 0; w < 8; w++)
				{
					W = w;
					H = h;

					wrappedX = (wrap == TRUE) ? ((X + W+(i*8)) % screenWidth < 0) ? screenWidth + (X + W+(i*8)) % screenWidth : (X + W+(i*8)) % screenWidth : X + W +(i*8);
					wrappedY = (wrap == TRUE) ? ((Y + H+(j*8)) % screenHeight < 0) ? screenHeight + (Y + H+(j*8)) % screenHeight : (Y + H+(j*8)) % screenHeight : Y + H +(j*8);

					if (wrap || est_dans_ecran(wrappedX, wrappedY))
					{
						int pixelIndex = (w + h * 8) / 4;

						frameIndex = (wrappedY * screenWidth + wrappedX) / 4;
						remainder = W % 4;
						pos = (remainder == 0 ? 1 : (remainder == 1 ? 2 : (remainder == 2 ? 3 : 4)));

						unpackValues(final[pixelIndex], &color, pos);
						if (color != 3)
						{
							shift = 6 - (wrappedX % 4) * 2;
							Frame_buffer[frameIndex] = (Frame_buffer[frameIndex] & ~(0x03 << shift)) | (color << shift);
						}
					}
				}
			}
		}
	}	
}



void dessiner_tile_in_memory(int X, int Y)
{
	int D = (60 * Y) + (X / 4);
	char k;

	switch (X % 4)
	{
		case 0:
			for (k = 0; k < 8; k++)
			{
				Frame_buffer[D + 60 * k] = final[2 * k];
				Frame_buffer[D + 60 * k + 1] = final[2 * k + 1];
			}
			break;
		default:
			break;
	}
}


void dessiner_tile(int X, int Y, int N, SYM S)
{
	recuperer_tile(N, S);
	dessiner_tile_in_memory(X, Y);
}

/**
 * Charge le buffer d'affichage dans la mémoire d'accès aléatoire graphique (GRAM).
 *
 * Parcourt chaque pixel du buffer d'affichage, décompresse les valeurs de couleur,
 * puis écrit ces valeurs dans la GRAM.
 *
 * Utilise la fonction unpackValues pour décompresser les valeurs de couleur,
 * et la fonction write_data pour écrire les valeurs dans la GRAM.
 */
void charger_GRAM()
{
	int h, w;
	unsigned char var1;
	unsigned char var2;
	unsigned char var3;
	unsigned char var4;
	for (h = 0; h < 320; h++)
	{
		for (w = 0; w < 240 / 4; w++)
		{
			unpackValues(Frame_buffer[w + h * 240 / 4], &var1, 1);
			unpackValues(Frame_buffer[w + h * 240 / 4], &var2, 2);
			unpackValues(Frame_buffer[w + h * 240 / 4], &var3, 3);
			unpackValues(Frame_buffer[w + h * 240 / 4], &var4, 4);
			write_data(COLORS[var1]);
			write_data(COLORS[var2]);
			write_data(COLORS[var3]);
			write_data(COLORS[var4]);
		}
	}
}

/**
 * Affiche la carte de tuiles sur l'écran.
 *
 * Parcourt chaque tuile de la carte de tuiles, puis dessine cette tuile à la position appropriée sur l'écran.
 *
 * Utilise la fonction dessiner_tile pour dessiner chaque tuile.
 */
void afficher_tilemap()
{
	int h, w;
	int N;
	for (h = 0; h < 320 / 8; h++)
	{
		for (w = 0; w < 240 / 8; w++)
		{
			N = w + h * 30;
			dessiner_tile(w * 8, h * 8, N, NO_SYM);
		}
	}
}
	
void dessiner_george(unsigned char X ,int i){
	int Y = 256; // hauteur du vaisseau
	if (i <= 2) {
		dessiner_sprite_george(spacecraft_frame_1,X,Y,TRUE);
	}
	else if (i <= 5 && i > 2) {
		dessiner_sprite_george(spacecraft_frame_2,X,Y+1,TRUE);
	}
	else if (i <= 8 && i > 5) {
		dessiner_sprite_george(spacecraft_frame_3,X,Y+1,TRUE);
	}
	else if (i <= 11 && i > 8) {
		dessiner_sprite_george(spacecraft_frame_4,X,Y-1,TRUE);
	}
	else if (i <= 14 && i > 11) {
		dessiner_sprite_george(spacecraft_frame_5,X,Y-1,TRUE);
	}
}

void afficher_historique(void){
	int i;
	unsigned char id_sort;
	short hauteur;
	const SPRITE *sprite = &Champ_meteorites;
	for (i = 0; i < 10; i++){
		hauteur = historique[i].hauteur;
		id_sort = historique[i].id_sort;
		if (historique[i].var_affichage == 1){
			sprite = &Sombronces;
		} else {
			sprite = &Champ_meteorites;
		}
			switch (id_sort){
			case 21 :
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_george(*sprite,COL4,hauteur,FALSE);
				break;
			case 22 :
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_george(*sprite, COL3, hauteur, FALSE);
				break;
			case 23:
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_george(*sprite, COL2, hauteur, FALSE);
				break;
			case 24:
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_george(*sprite, COL1, hauteur, FALSE);
				break;
			case 41 : 
				sprite = &Sablieres;
				dessiner_sprite_george(*sprite,COL3,hauteur,FALSE);
				break;
			case 43 :
				sprite = &Sablieres;
				dessiner_sprite_george(*sprite, COL2, hauteur, FALSE);
				break;
			case 46 :
				sprite = &Sablieres;
				dessiner_sprite_george(*sprite, COL1, hauteur, FALSE);
				break;
			case 42 : 
				dessiner_sprite_george(Champ_meteorites,COL2,hauteur,FALSE);
				dessiner_sprite_george(Champ_meteorites,COL4,hauteur,FALSE);
				break;
			case 44 :
				dessiner_sprite_george(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 45 :
				dessiner_sprite_george(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL3, hauteur, FALSE);
				break;
			case 71 :
				dessiner_sprite_george(Champ_meteorites, COL2, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL3, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 72 :
				dessiner_sprite_george(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL3, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 73 :
				dessiner_sprite_george(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL2, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 74 :
				dessiner_sprite_george(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL2, hauteur, FALSE);
				dessiner_sprite_george(Champ_meteorites, COL3, hauteur, FALSE);
				break;
			default :
				break;
		}
	}
}

void dessiner_sur_grille(SPRITE sprite, int X, int Y){
	int i, j;
	unsigned char sprite_hauteur = sprite.tile_hauteur;
	unsigned char sprite_largeur = sprite.tile_largeur; 
	for (j = 0; j < sprite_hauteur; j++){
		for (i = 0; i < sprite_largeur; i++){
			dessiner_tile(X + i * 8, Y + j * 8, sprite.sprite_tiles[i + j * sprite_largeur], (SYM)sprite.sym_tiles[i + j * sprite_largeur]);
		}
	}
}

void effacer_buffer_header(){
	unsigned short h,w;
	char color = packValues(0, 0, 0, 0);
	for (h = 0; h < 24 ;h++){
		for (w = 0; w < 240/4; w++){
			Frame_buffer[w + h * 240 / 4] = color;
		}
	}
	dessiner_sur_grille(Score,11*8,0);
}

void dessiner_chiffre(int X, int Y, int chiffre){
	switch (chiffre){
		case 0 :
			dessiner_tile(X,Y,90,NO_SYM);
			break;
		case 1 :
			dessiner_tile(X,Y,91,NO_SYM);
			break;
		case 2 :
			dessiner_tile(X,Y,92,NO_SYM);
			break;
		case 3 :
			dessiner_tile(X,Y,120,NO_SYM);
			break;
		case 4 :
			dessiner_tile(X,Y,121,NO_SYM);
			break;
		case 5 :
			dessiner_tile(X,Y,122,NO_SYM);
			break;
		case 6 :
			dessiner_tile(X,Y,150,NO_SYM);
			break;
		case 7 :
			dessiner_tile(X,Y,151,NO_SYM);
			break;
		case 8 :
			dessiner_tile(X,Y,152,NO_SYM);
			break;
		case 9 :
			dessiner_tile(X,Y,150,NO_SYM);
			break;
		default :
			break;
	}
}

void afficher_score(int score){
	unsigned char unite = score % 10;
	unsigned char dizaine = (score / 10) % 10;
	unsigned char centaine = (score / 100) % 10;
	unsigned char millier = (score / 1000) % 10;
	unsigned char dix_millier = (score / 10000) % 10;
	dessiner_chiffre(17*8,8,0);
	dessiner_chiffre(16*8,8,unite);
	dessiner_chiffre(15*8,8,dizaine);
	dessiner_chiffre(14*8,8,centaine);
	dessiner_chiffre(13*8,8,millier);
	dessiner_chiffre(12*8,8,dix_millier);
}

void dessiner_explosion(int X, int Y,int i){
	if (i <= 2){
		dessiner_sprite_george(Prout_frame_1,X,Y+24,FALSE);
	} else if (i <= 5 && i > 2){
		dessiner_sprite_george(Prout_frame_2,X-8,Y+24,FALSE);
	} else if (i <= 8 && i > 5){
		dessiner_sprite_george(Prout_frame_3,X-16,Y+16,FALSE);
	} else if (i <= 11 && i > 8){
		dessiner_sprite_george(Prout_frame_6,X-24,Y+8,FALSE);
	} else if ( i > 11){
		dessiner_sprite_george(Prout_frame_7,X-24,Y+8,FALSE);
	}
}
