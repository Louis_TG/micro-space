#ifndef SON_H
#define SON_H

void InitSpeaker(void);
void InitTimer1(void);
void TIMER0_IRQHandler(void);
void TIMER1_IRQHandler(void);
void JouerUneNote(float f);
void JouerMusique(void);

#endif
