//===========================================================//
// Projet Micro - SNUM1 - ENSSAT - S2 2024   							   //
//===========================================================//
// File                : Micro_Space
// Hardware Environment: Open1768
// Build Environment   : Keil µVision
//===========================================================//

#include "main.h"
#include "globaldec.h"

//===========================================================//
// 						Function: Main
//===========================================================//

int main(void){
	int i = 0;
  multiples_init();
	
	while (!crash){
		if(Flag_capteur){
			ADC_CONTROL_REGISTER |= (1<<24); // On lance une acquisition MAINTENANT
			Flag_capteur = FALSE;
			lecture_ADC();
		  acquisition_capteur();
		  deplacement_joueur();
		}
		
		if(Flag_affichage){
			i += 1;
			i %= 15;
			effacer_buffer();
			afficher_score(score);
			afficher_historique();
			dessiner_george(position_joueur,i);
			charger_GRAM(); // 33.6 ms latency to write all that
			collision();
			maj();
			if(vitesse_augmenter){maj();}
			Flag_affichage = FALSE;
		}
		
		if(Flag_mana){
			Flag_mana = FALSE;
			AffichageMana();
		}
		
		if(Flag_message){ // Si un message bluetooth est detecte (mis a 1 dans l'interruption)
			Flag_message = FALSE; // on repasse le messagebluetooth a 0
			DiminutionMana();
			ajout_nouveau_sort();
		}
	}
	i = 0;
	while (i < 20){
		i++;
		effacer_buffer();
		dessiner_george(position_joueur,0);
		afficher_historique();
		dessiner_explosion(position_joueur,234,i);
		charger_GRAM();
	}
}


//---------------------------------------------------------------------------------------------	
#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
#endif
