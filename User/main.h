#ifndef MAIN_H
#define MAIN_H

#include "lpc17xx_libcfg_default.h"						// check_failed
#include "touch\ili_lcd_general.h"						// Driver affichage

#include "affichagelcd.h"											// Affichage
#include "touch\timer_kivabien.h"							// Timer 2 et 3
#include "communicationbluetooth.h"						// Module bluetooth
#include "touch\CAD.h"												// ADC
#include "touch\capteurs.h"										// Capteurs
#include "GestionMana.h"											// Synchronisation mana avec le tel
#include "son.h"															// Musique
#include "historique.h"												// Gestion des sorts en m�moire
#include "touch\sort.h"												// Aleatoire des sorts
#include "initialisation.h"										// Fait la routine d'initialisation du jeu

#endif
