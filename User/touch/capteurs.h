#ifndef CAPTEUR_CONTACT_H
#define CAPTEUR_CONTACT_H

void initialisation_capteur(void);
void acquisition_capteur(void);
void deplacement_joueur(void);

#endif
