#include "timer_kivabien.h"
#include "lpc17xx_timer.h"
#include "global.h"


void initialisation_timer(){
  TIM_TIMERCFG_Type maconfig;
  TIM_MATCHCFG_Type maconfigmatch;
	
	// -----Gestion du timer 2 pour les capteurs et le mana-----
  maconfig.PrescaleOption = TIM_PRESCALE_USVAL;							// Decompte du temps en microsecondes
  maconfig.PrescaleValue = 1000;														// Valeur de 1000 pour un decompte du temps en millisecondes
  TIM_Init(LPC_TIM2, TIM_TIMER_MODE, &maconfig);
  
	// Pour les capteurs
  maconfigmatch.MatchChannel = 1;
  maconfigmatch.IntOnMatch = ENABLE;
  maconfigmatch.StopOnMatch = DISABLE;
  maconfigmatch.ResetOnMatch = DISABLE;											// C'est le plus court des 2 interruptions donc pas de reset
  maconfigmatch.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
  maconfigmatch.MatchValue = TEMP_MS_CAPTEUR;
  TIM_ConfigMatch(LPC_TIM2, &maconfigmatch);
	
	// Pour le mana
  maconfigmatch.MatchChannel = 0;
  maconfigmatch.IntOnMatch = ENABLE;
  maconfigmatch.StopOnMatch = DISABLE;
  maconfigmatch.ResetOnMatch = ENABLE;
  maconfigmatch.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
  maconfigmatch.MatchValue = TEMP_MS_MANA;									// C'est le plus long donc c'est ici qu'on fait un reset
  TIM_ConfigMatch(LPC_TIM2, &maconfigmatch);
	
	NVIC_EnableIRQ(TIMER2_IRQn);															// On permet au timer 2 de faire des interruptions
  TIM_Cmd(LPC_TIM2, ENABLE);																// On lance le timmer 2
  
  // -----Gestion du timer 3 pour l'affichage-----
	TIM_Init(LPC_TIM3, TIM_TIMER_MODE, &maconfig);						// Egalement un decompte en milliseconde
	
	maconfigmatch.MatchChannel = 0;
  maconfigmatch.ResetOnMatch = ENABLE;
  maconfigmatch.MatchValue = 1;
	TIM_ConfigMatch(LPC_TIM3, &maconfigmatch);
	
	NVIC_EnableIRQ(TIMER3_IRQn);															// On permet au timer 3 de faire des interruptions
  TIM_Cmd(LPC_TIM3, ENABLE);																// On lance le timmer 3
}

void TIMER2_IRQHandler(){
	if(TIM_GetIntStatus(LPC_TIM2,TIM_MR1_INT) == SET){				// Si on est en interruption a cause du channel 1
	  Flag_capteur = TRUE;
		incrementeur_capteur_mana++;														// On incremente le multiplicateur
		MATCH21 = TEMP_MS_CAPTEUR*incrementeur_capteur_mana;		// On met a jour la valeur de match du timer 2 channel 1
	  TIM_ClearIntPending(LPC_TIM2, TIM_MR1_INT);							// On fait l'acquitement
	}
	if(TIM_GetIntStatus(LPC_TIM2,TIM_MR0_INT) == SET){				// Si on est en interruption a cause du channel 2
	  Flag_mana = TRUE;
    if(mana <25){mana++;}																		// On incremente le mana avec un max de 25
		score++;																								// On incremente le score
		incrementeur_capteur_mana = 1;													// On remet le multiplicateur  1
		MATCH21 = TEMP_MS_CAPTEUR*incrementeur_capteur_mana;		// On met  jour la valeur de match du timer 2 channel 1
		TIM_ClearIntPending(LPC_TIM2, TIM_MR0_INT);							// On fait l'acquitement
	}
}

void TIMER3_IRQHandler(){
	Flag_affichage = TRUE;
  TIM_ClearIntPending(LPC_TIM3, TIM_MR0_INT);
}
