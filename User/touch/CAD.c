#include "stdint.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_adc.h"
#include "CAD.h"
#include "global.h"

void init_ADC(){
	PINSEL_CFG_Type config;												// Pour la configuration à venir du PINSEL
	
	//Configuration de l'ADC en lui-même
	PCOMP |= (1<<12);															// on permet l'utilisation d'énergie pour l'ADC
  ADC_CONTROL_REGISTER = 0x0; 									// Initialement le registre est à 0 mais pour être sur on le remet à 0
	ADC_CONTROL_REGISTER |= (1<<1); 							// On va faire une acquisition sur le pin AD0.1
																								// On laisse les autres Pin (0 et 2 à 7) à 0 pour faire une acquisition en software controlled mode
	ADC_CONTROL_REGISTER |= (1<<8);								// On met se bit à 1 afin de diviser la fréquence d'horloge recue par l'ADC (par défaut 25MHz) par une valeur
																								// Les bits 8 à 15 forment cette valeur (ici 1) à laquelle est ajouté 1 pour faire la division. On a donc une horloge à 12.5 MHz
																								// Le bit 16 est à 0 donc on est en software controlled mode
	ADC_CONTROL_REGISTER |= (1<<21);							// On allme l'ADC
	
	// Configuration de la P0.24 pour être la pin d'entré de la tension analogique que l'ADC converti avec AD0.1
	config.Portnum = 0;
	config.Pinnum = 24;
	config.Funcnum = 1;
	config.Pinmode = PINSEL_PINMODE_PULLUP;
	config.OpenDrain = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&config);
}

void lecture_ADC(){
	uint32_t data;
	data = (ADC_GLOBAL_DATA_REGISTER >> 4) & 0xFFF;
	if(data < 400){Capteur_moisissure = FALSE;} // On fait un seuil pour déclencher le bool du capteur
	else{Capteur_moisissure = TRUE;}
}
