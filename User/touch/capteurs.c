#include "capteurs.h"
#include "lpc_types.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "core_cm3.h"
#include "global.h"

void initialisation_capteur(){
	PINSEL_CFG_Type config;
	config.Portnum = 0; // Configuration de P0.0 pour le capteur de contact
	config.Pinnum = 0;
	config.Funcnum = 0;
	config.Pinmode = PINSEL_PINMODE_PULLDOWN; // On configure un PULLDOWN, ainsi lorsque le capteur sera appuy� on lira un 1 et 0 sinon
	config.OpenDrain = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&config);
  GPIO_SetDir(0, 0x1, 0); // Bit 0 pour le capteur de contact
}

void acquisition_capteur(){
	uint32_t lecture_capteur; // Pour le capteur de contact
	lecture_capteur = GPIO_ReadValue(0); // On lit le capteur de contact
	if(lecture_capteur & (1<<0)){Capteur_contact = TRUE;} // Si c'est un 1, c'est qu'il y a un contact donc capteur = TRUE
	else{Capteur_contact = FALSE;} // Sinon c'est 0
}

void deplacement_joueur(){
  if(Capteur_contact){
	  if(position_joueur > 8){position_joueur = position_joueur - 8;}
	}
	if(Capteur_moisissure){
	  if(position_joueur < 208){position_joueur = position_joueur + 8;}
	}
}
