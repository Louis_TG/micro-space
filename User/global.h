#ifndef GLOBAL_H
#define GLOBAL_H

#include "constantes.h"

extern Bool Flag_capteur;															// Flag pour l'echantillonage des capteurs d'humidite et de contact
extern Bool Flag_affichage;														// Flag pour declencher un nouvel affichage
extern Bool Flag_mana;																// Flag pour l'affichage du mana
extern Bool Flag_message;															// Flag pour traiter un message bluetooth recu
extern Bool Capteur_contact; 													// Capteur de contact appuye ou non
extern Bool Capteur_moisissure; 											// Capteur de moisisure appuye ou non
extern Bool vitesse_augmenter;												// Pour savoir quand la vitesse est doublee
extern Bool crash;																		// Pour savoir quand la partie est finie
extern unsigned char mana;														// Valeur du mana
extern unsigned short score;													// Valeur du score
extern unsigned char position_joueur; 								// Position du vaisseau en pixel (8 a 208)
extern unsigned char sort;														// Cout du sort lance puis identifiant de ce sort
extern unsigned char incrementeur_capteur_mana; 			// Permet la gestion d'une interruption pour les capteurs et le mana avec UN SEUL timer
extern char MessageRecu;															// Variable pour stocker le message recu (sort lance)
extern char MessageEnvoye[10];												// Variable pour stocker le message envoye (valeur de mana)
extern char Frame_buffer[(240 / 4) * 320];						// Buffer pour l'affichage LCD
extern float vitesse_musique;													// Permet d'accelerer la musique si la vitesse est doublee
extern el_histo historique[NOMBRE_SORT];							// Permet de garder en memoire les sorts pour gerer l'affichage et les collisions

#endif
