/*===========================================================================================================================*/
/* INCLUSION DES FICHIERS UTILES */
/*===========================================================================================================================*/

#include "lpc17xx_gpio.h"
#include "lpc17xx_timer.h"
#include "pitches.h"
#include "global.h"
#include "son.h"

/*===========================================================================================================================*/
// D�claration des variables
/*===========================================================================================================================*/

Bool FlagTimer = FALSE ;															// �tat du timer							
int NoteFaite = 0 ;
extern int PacmanDuree[] ;
extern int PacmanTheme[] ;
extern int marioDuree[] ;
extern int marioTheme[] ;
extern int SubwayDuree[] ;
extern int SubwayTheme[] ;
extern int OverDuree[] ;
extern int OverTheme[] ;

typedef enum {Pacman, Mario, Subway, Over} Musique ;

TIM_MATCHCFG_Type speakermatch ; 											// D�claration de la variable speakermatch
TIM_MATCHCFG_Type timer1match ; 											// D�claration de la variable timer1match

// Variables pour pouvoir changer la musique
Bool finPacman = FALSE ;
Bool finMario = TRUE ;
Bool finSubway = TRUE ;
Bool fin = TRUE ;

int i = 0 ;


/*===========================================================================================================================*/
/* PROGRAMMATION DU SPEAKER POUR LA MUSIQUE DE FOND (TIMER0) */
/*===========================================================================================================================*/

void InitSpeaker(){
	TIM_TIMERCFG_Type speaker ; 													// D�claration de la variable myconfig
	
	GPIO_SetDir(0, 1<<26, 1);															// on met le speaker en sortie
	speaker.PrescaleOption = TIM_PRESCALE_USVAL ;					// configuration du registre PR -> valeur en microseconde
	speaker.PrescaleValue = 1	; 													// Pr�cision � la microseconde
	
	TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &speaker) ;				// configuration du Timer
	
	/* CONFIGURATION DE LA VARIABLE speakermatch */
	speakermatch.MatchChannel = 0 ;
	speakermatch.ResetOnMatch = ENABLE ;									// Reset sur le match autoris�
	speakermatch.StopOnMatch = DISABLE ;									// Stop sur le match non autoris�
	speakermatch.IntOnMatch = ENABLE ;
	speakermatch.MatchValue = 10 ;												// valeur du match
	speakermatch.ExtMatchOutputType = 0 ;
	
	TIM_ConfigMatch(LPC_TIM0, &speakermatch) ;						// Configuration du match
	
	NVIC_EnableIRQ(TIMER0_IRQn) ; 												// Autorisation des interruptions pour le timer0
	
	TIM_Cmd(LPC_TIM0, ENABLE) ; 													// Demarrage du Timer
}

/*===========================================================================================================================*/
/* ROUTINE D'INTERRUPTION DU TIMER0 */ 
/*===========================================================================================================================*/

void TIMER0_IRQHandler(){																// Nom de fonction � respecter sinon pas d'int�rruption possible
	
	if(!FlagTimer){
		GPIO_SetValue(0, 1 << 26) ;
		FlagTimer = TRUE ;
	}
	else {
		GPIO_ClearValue(0, 1 << 26) ;
		FlagTimer = FALSE ;
	}
	
	TIM_ClearIntPending(LPC_TIM0,TIM_MR0_INT) ; 						// Acquittement (sinon interruption bizarre : non respect des 10ms)
}

/*===========================================================================================================================*/
/* PROCEDURE POUR JOUER UNE NOTE EN UTILISANT LE MATCH DU TIMER0 */
/*===========================================================================================================================*/

void JouerUneNote(float f){

   	speakermatch.MatchValue = (int)((1/f)*1000000);				// jouer la note en mettant le timer dans la bonne unit� 
	
		TIM_ConfigMatch(LPC_TIM0,&speakermatch);
   	TIM_ResetCounter(LPC_TIM0);   	 											// On reset le timer	 
		TIM_Cmd(LPC_TIM0, ENABLE);   	 												// On allume le timer 	
}

/*===========================================================================================================================*/
/* INITIALISATION DU TIMER 1 : permet de jouer la musique en int�rruption */
/*===========================================================================================================================*/

void InitTimer1(){
	TIM_TIMERCFG_Type timer1 ; 														// D�claration de la variable timer1
	
	timer1.PrescaleOption = TIM_PRESCALE_USVAL ;					// configuration du registre PR -> valeur en microseconde
	timer1.PrescaleValue = 1	; 													// Pr�cision � la microseconde
	
	TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &timer1) ;					// configuration du Timer
	
	/* CONFIGURATION DE LA VARIABLE timermatch */
	timer1match.MatchChannel = 1 ;
	timer1match.ResetOnMatch = ENABLE ;										// Reset sur le match autoris�
	timer1match.StopOnMatch = DISABLE ;										// Stop sur le match non autoris�
	timer1match.IntOnMatch = ENABLE ;
	timer1match.MatchValue = PacmanDuree[0] ;							// valeur du match
	timer1match.ExtMatchOutputType = 0 ;
	
	TIM_ConfigMatch(LPC_TIM1, &timer1match) ;							// Configuration du match
	TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
	
	NVIC_EnableIRQ(TIMER1_IRQn) ; 												// Autorisation des interruptions pour le timer0
	
	TIM_Cmd(LPC_TIM1, ENABLE) ; 													// D�marrage du Timer
}


/*===========================================================================================================================*/
/* ROUTINE D'INTERRUPTION DU TIMER1 */ 
/*===========================================================================================================================*/

void TIMER1_IRQHandler(){
	if(!finPacman){
		timer1match.MatchValue = vitesse_musique*1.1*10000*PacmanDuree[NoteFaite] ; 
		TIM_ConfigMatch(LPC_TIM1,&timer1match);
		TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
		TIM_Cmd(LPC_TIM1, ENABLE);   	 												// On allume le timer 
	
		JouerUneNote(2*PacmanTheme[NoteFaite]);
			
		NoteFaite = NoteFaite + 1 ; 
		if(NoteFaite > 30){
			NoteFaite = 0 ;					// on r�initialise le compteur
			i++ ;
			if(i%3 == 0){
				finPacman = TRUE ;
				finMario = TRUE ;
				finSubway = FALSE ;
				fin = TRUE ;
			}
		}
	}

	
		/*else if(!finMario){
			timer1match.MatchValue = 0.4*100000*marioDuree[NoteFaite] ; 
			TIM_ConfigMatch(LPC_TIM1,&timer1match);
			TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
			TIM_Cmd(LPC_TIM1, ENABLE);   	 												// On allume le timer 
	
			JouerUneNote(2*marioTheme[NoteFaite]);
		
			NoteFaite = NoteFaite + 1 ; 
			if(NoteFaite > 285){
				finPacman = TRUE ;
				finMario = TRUE ;
				finSubway = FALSE ;
				fin = TRUE ;
				NoteFaite = 0 ;																			// on r�initialise le compteur
			}
		}*/
			

		if(!finSubway){
			timer1match.MatchValue = vitesse_musique*10000*SubwayDuree[NoteFaite] ;
			TIM_ConfigMatch(LPC_TIM1,&timer1match);
			TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
			TIM_Cmd(LPC_TIM1, ENABLE);   	 												// On allume le timer 
	
			JouerUneNote(2*SubwayTheme[NoteFaite]);
	
			NoteFaite = NoteFaite + 1 ; 
			if(NoteFaite > 60){
				NoteFaite = 0 ;																			// on r�initialise le compteur
				i++ ;
				if(i%3 == 0){
					finPacman = FALSE ;
					finMario = TRUE ;
					finSubway = TRUE ;
					fin = TRUE ;
				}
			}
		}

		/*else if(!fin){
			timer1match.MatchValue = 0.4*100000*OverDuree[NoteFaite] ; 
			TIM_ConfigMatch(LPC_TIM1,&timer1match);
			TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
			TIM_Cmd(LPC_TIM1, ENABLE);   	 												// On allume le timer 
	
			JouerUneNote(2*OverTheme[NoteFaite]);
		
			NoteFaite = NoteFaite + 1 ; 
			if(NoteFaite > 11){
				NoteFaite = 0 ;																			// on r�initialise le compteur
			}
		}*/

	TIM_ClearIntPending(LPC_TIM1,TIM_MR1_INT) ; 							// Acquittement 
}



