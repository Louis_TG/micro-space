import numpy as np

# Define color palette
palette = {
    0x08c4: 0,    # Black
    0x334a: 1,    # Dark_Green
    0x85ed: 2,    # Slime
    0xdfb9: 3     # Light Slime
}

# Calculate the Euclidean distance between two colors
def color_distance(color1, color2):
    return np.linalg.norm(np.array(color1) - np.array(color2))

# Function to find the closest color in the palette for a given color
def closest_color(color):
    distances = [(palette_color, color_distance(color, palette_color)) for palette_color in palette]
    closest_color = min(distances, key=lambda x: x[1])[0]
    return closest_color

# Input and output file paths
input_file_path = "input_file.txt"
output_file_path = "input_file_1.txt"

# Read input file
with open(input_file_path, "r") as f:
    lines = f.readlines()

# Process data and transform using palette
transformed_data = []
for line in lines:
    line = line.replace("static const uint16_t", "const char")
    if "{" in line or "}" in line:
        transformed_data.append(line)
    else:
        pixels = line.strip().split(",")
        transformed_pixels = [palette[closest_color(int(pixel.strip(), 16))] for pixel in pixels if pixel.strip()]
        transformed_data.append(", ".join(str(pixel) for pixel in transformed_pixels))

# Write transformed data to output file
with open(output_file_path, "w") as f:
    for index, line in enumerate(transformed_data):
        print(line + "\n ---")
        if index < len(transformed_data) - 1:
            next_line = transformed_data[index + 1]
            if next_line.strip() and next_line.strip()[0] in '0123456789' and line[0] != "c":
                f.write(line.rstrip() + ",\n")
            else:
                f.write(line.rstrip() + "\n")
        else:
            if len(line) > 0 and line.strip()[-1] == '{':  # Check if the last non-empty line ends with '{'
                f.write(line.rstrip() + "\n")  # Write without comma and with newline character
            else:
                f.write(line.rstrip())  # For the last line, no newline character is needed


print("Transformation complete. Output saved to:", output_file_path)
